﻿#pragma strict

private var power = 800; //포탄 발사 속도
var bullet : Transform;   //포탄
var target : Transform;   //LootAt() 목표
var spPoint : Transform;  //spawnPoint
var explosion : Transform;//포구 앞의 화염

private var ftime : float = 0.0;

function Update () {
	transform.LookAt(target);
	ftime += Time.deltaTime;
	
	var hit : RaycastHit;
	var fwd = transform.TransformDirection(Vector3.forward);
	
	
	//Debug.DrawRay(spPoint.transform.position, fwd * 20, Color.green);
	if (Physics.Raycast(spPoint.transform.position, fwd, hit, 20) == false)
	{
	return;
	//Debug.Log(hit.collider.gameObject.name);
	}
	
	if (hit.collider.gameObject.tag != "Tank" || ftime < 1.7) return;

	
	
	Instantiate(explosion, spPoint.transform.position, spPoint.transform.rotation);
	var obj = Instantiate(bullet, spPoint.transform.position, Quaternion.identity);
	obj.rigidbody.AddForce(fwd * power);
	ftime = 0;
	
	}
	