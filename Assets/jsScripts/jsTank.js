﻿#pragma strict

var speed = 3;  //이동속도(m/s)
var rotSpeed = 150;  //회전속도 (각도/s)

var turret : GameObject; //포탑

var power = 800;            // 포탄 발사 속도
var bullet : Transform;     //포탄
var explosion : Transform;

function Update() {
	var amtToMove = speed * Time.deltaTime; // 프레임에 이동할 거리
	var amtToRot = rotSpeed * Time.deltaTime; //  프레임에 회전할 각도
	
	var front = Input.GetAxis("Vertical"); // 전후진
	var ang = Input.GetAxis("Horizontal");  // 좌우 회전 방향
	
	var ang2 = Input.GetAxis("MyTank");   // 포탑 회전 방향(벡터) 추가
	
	transform.Translate(Vector3.forward * front * amtToMove); // 탱크 전후진
	transform.Rotate(Vector3(0, ang * amtToRot, 0));  //탱크 회전

	turret.transform.Rotate(Vector3.up * ang2 * amtToRot); // 포탑 회전 추가

	if (Input.GetButtonDown("Fire1")) {
		var spPoint = GameObject.Find("spawnPoint");
		Instantiate(explosion, spPoint.transform.position, Quaternion.identity);
		var myBullet = Instantiate(bullet, spPoint.transform.position, spPoint.transform.rotation);
		myBullet.rigidbody.AddForce(spPoint.transform.forward * power);
	}
}
	