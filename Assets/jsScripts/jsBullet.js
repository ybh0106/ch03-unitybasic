﻿#pragma strict

var explosion : Transform;

function OnTriggerEnter(coll : Collider) {
	Instantiate(explosion, transform.position, Quaternion.identity);
	
	
	//Destroy(gameObject);
	
	if (coll.gameObject.tag == "Wall") {
	
		Destroy(coll.gameObject);
	} else if (coll.gameObject.tag == "Enemy") {
	// 나중에 추가할 부분
		jsScore.hit++;
		if(jsScore.hit > 5) 
		{
			Destroy(coll.transform.root.gameObject);
			//승리화면으로
			Application.LoadLevel("WinGame");
		}
	} else if (coll.gameObject.tag == "Tank") {
	// 나중에 추가
		jsScore.lose++;
		if(jsScore.lose > 5)
		{
			//패배화면으로 분기
			Application.LoadLevel("LoseGame");
		}
	}
}