﻿#pragma strict

var rotAng = 270;

function Update () {
	var amtToRot = rotAng * Time.deltaTime;
	transform.RotateAround(Vector3.zero, Vector3.up, amtToRot);
}